FROM sonarqube:9.7.1-community

COPY sonar-cxx-plugin-2.1.0.253.jar /opt/sonarqube/extensions/plugins/
COPY sonar-auth-oidc-plugin-2.1.1.jar /opt/sonarqube/extensions/plugins/
COPY sonar-sql-plugin-1.2.4.jar /opt/sonarqube/extensions/plugins/
COPY sonar-flutter-plugin-0.4.0.jar /opt/sonarqube/extensions/plugins/